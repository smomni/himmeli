from setuptools import setup, find_packages
from himmeli import __version__

setup(
    name='himmeli',
    version=__version__,
    packages=find_packages(),
    extras_require={
        'dev': [],
        'test': ['pytest', 'pytest-cov'],
        'prod': []
    },
    scripts=['dummy-producer.py']
)