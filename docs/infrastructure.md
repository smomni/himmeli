# himmeli-infrastructure
Description goes here...

![](infrastructure.png)

## Producers

Description goes here...

## Data ingestion

Data is ingested by [Apache Kafka](https://kafka.apache.org/).

## Stream processing

Stream processing by [Kafka Streams](https://kafka.apache.org/documentation/streams/).

## Data store

Data is stored in an [InfluxDB](https://www.influxdata.com/) time-series database.

## Actuators

Description goes here...